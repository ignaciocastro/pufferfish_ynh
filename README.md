# Minecraft Pufferfish Fork
This Yunohost package will let you run an optimized Minecraft server on Yunohost.

## Requirements
* A root SSH access to your server
* A server based on Debian

## Installtion

### On YunoHost

```bash
sudo yunohost app install https://gitea.com/ignaciocastro/pufferfish_ynh
sudo yunohost firewall allow TCP 25565
sudo yunohost firewall allow TCP 25575
```

### On any Debian-based system

```bash
curl https://gitea.com/ignaciocastro/pufferfish_ynh/raw/branch/master/scripts/install | bash
```

## Change the settings of the game
If you already know how to change the settings of a Minecraft Server using `server.propeties` and others.
You can do it into the folder `/home/yunohost.app/pufferfish`.

Don't forget to run the following command to apply your changes: `systemctl restart pufferfish`. 

## Manage your server

* `systemctl start pufferfish` : Start the server
* `systemctl stop pufferfish` : Stop the server
* `systemctl restart pufferfish` : Restart the server
* `systemctl enable pufferfish` : Makes the server auto-start when booting (default)
* `systemctl disable pufferfish` : Do the opposite. 
* `systemctl status pufferfish` : Check the status of the app

## Uninstall
### On YunoHost

```bash
sudo yunohost app remove pufferfish
```

### On any Debian-based system

```bash
curl https://gitea.com/ignaciocastro/pufferfish_ynh/raw/branch/master/scripts/remove | bash
```

